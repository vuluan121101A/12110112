namespace Blog_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MyBlog4 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.BaiViet", "Title", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.BaiViet", "Body", c => c.String(nullable: false, maxLength: 1000));
            AlterColumn("dbo.Comments", "ID", c => c.Int(nullable: false));
            AlterColumn("dbo.Comments", "Body", c => c.String(nullable: false, maxLength: 1000));
            AlterColumn("dbo.Comments", "Author", c => c.String(nullable: false));
            AlterColumn("dbo.Tags", "TagID", c => c.Int(nullable: false));
            AlterColumn("dbo.Tags", "Content", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Accounts", "AccountID", c => c.Int(nullable: false));
            AlterColumn("dbo.Accounts", "Password", c => c.String(nullable: false));
            AlterColumn("dbo.Accounts", "Email", c => c.String(nullable: false));
            AlterColumn("dbo.Accounts", "FirstName", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Accounts", "LastName", c => c.String(nullable: false, maxLength: 50));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Accounts", "LastName", c => c.String());
            AlterColumn("dbo.Accounts", "FirstName", c => c.String());
            AlterColumn("dbo.Accounts", "Email", c => c.String());
            AlterColumn("dbo.Accounts", "Password", c => c.String());
            AlterColumn("dbo.Accounts", "AccountID", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Tags", "Content", c => c.String());
            AlterColumn("dbo.Tags", "TagID", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Comments", "Author", c => c.String());
            AlterColumn("dbo.Comments", "Body", c => c.String());
            AlterColumn("dbo.Comments", "ID", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.BaiViet", "Body", c => c.String(nullable: false, maxLength: 250));
            AlterColumn("dbo.BaiViet", "Title", c => c.String(nullable: false));
        }
    }
}
